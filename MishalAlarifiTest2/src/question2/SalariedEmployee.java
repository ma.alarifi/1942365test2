package question2;

public class SalariedEmployee implements Employee{
	private double yearlyPay;
	
	public SalariedEmployee(double yearlyPay) {
		this.yearlyPay = yearlyPay;
	}
	
	public double getYearlyPay() {
		return yearlyPay;
	}
	
	public double getWeeklyPay() {
		return yearlyPay/52;
	}
}

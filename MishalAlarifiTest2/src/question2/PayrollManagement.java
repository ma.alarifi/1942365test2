package question2;

public class PayrollManagement {
	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(80000);
		employees[1] = new HourlyEmployee(40, 17.50);
		employees[2] = new UnionizedHourlyEmployee(40, 17.50, 35, 24.50);
		employees[3] = new SalariedEmployee(100000);
		employees[4] = new SalariedEmployee(65000);
		
		System.out.println(getTotalExpenses(employees));
	}
	
	public static double getTotalExpenses(Employee[] employees) {
		double expenses = 0;
		for(int i = 0; i < employees.length; i++) {
			expenses += employees[i].getWeeklyPay();
		}
		return expenses;
	}
}

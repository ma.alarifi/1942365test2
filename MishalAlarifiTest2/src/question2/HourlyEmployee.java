package question2;

public class HourlyEmployee implements Employee{
	private int hoursWorked;
	private double hourlyPay;
	
	public HourlyEmployee(int hoursWorked, double hourlyPay) {
		this.hoursWorked = hoursWorked;
		this.hourlyPay = hourlyPay;
	}
	
	public int getHoursWorked() {
		return hoursWorked;
	}
	
	public double getHourlyPay() {
		return hourlyPay;
	}
	
	public double getWeeklyPay() {
		return hoursWorked*hourlyPay;
	}
}

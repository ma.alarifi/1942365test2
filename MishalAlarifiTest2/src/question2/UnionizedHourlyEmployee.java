package question2;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(int hoursWorked, double hourlyPay, int maxHoursPerWeek, double overtimeRate) {
		super(hoursWorked, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
	
	public double getWeeklyPay() {
		if(this.getHoursWorked() <= maxHoursPerWeek) {
			return this.getHoursWorked()*this.getHourlyPay();
		}
		else {
			return this.getHourlyPay()*maxHoursPerWeek+overtimeRate*(this.getHoursWorked()-maxHoursPerWeek);
		}
	}
	
}

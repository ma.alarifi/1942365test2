package question3;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		ArrayList<Planet> result = new ArrayList<Planet>();
		for (Planet p: planets) {
			if(p.getRadius() >= size) {
				result.add(p);
			}
		}
		return result;
	}
}
